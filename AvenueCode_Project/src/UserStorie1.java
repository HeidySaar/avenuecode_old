import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;

public class UserStorie1 {
    
  private PageObject LoginSucess;
  private WebDriver driver;
  private String baseUrl;
  
  @Before
  public void setUp() throws Exception {
    driver = new FirefoxDriver();
    baseUrl = "http://qa-test.avenuecode.com/users/sign_in";
    this.LoginSucess = new PageObject(driver);
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void testAvenue() throws Exception {
	LoginSucess.Open();
	//Logger
	LoginSucess.Login("heidy_saar@hotmail.com", "avenue123");
	
	assertEquals("My Tasks", driver.findElement(By.xpath("(//a[contains(text(),'My Tasks')])[2]")).getText());
	driver.findElement(By.xpath("(//a[contains(text(),'My Tasks')])[2]")).click();
    driver.findElement(By.id("new_task")).sendKeys("Task 1");
    driver.findElement(By.xpath("//div[2]/span")).click();
    driver.findElement(By.linkText("Task 1")).click();
    driver.findElement(By.cssSelector("button.btn.btn-primary")).click();
    driver.findElement(By.xpath("//td[4]/button")).click();
    driver.findElement(By.id("new_sub_task")).sendKeys("Subtask 1");
    driver.findElement(By.id("add-subtask")).click();
    driver.findElement(By.cssSelector("div.modal-footer.ng-scope > button.btn.btn-primary")).click();
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
  }
}
