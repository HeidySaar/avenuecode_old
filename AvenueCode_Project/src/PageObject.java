import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class PageObject {
	private WebDriver driver;
	
	public PageObject(WebDriver driver) {
		this.driver = driver;
	}
		
	public void Open(){
		driver.navigate().to("http://qa-test.avenuecode.com/users/sign_in");
	}
	
	public void Login(String username, String password){
		driver.findElement(By.id("user_email")).sendKeys(username);
		driver.findElement(By.id("user_password")).sendKeys(password);
		driver.findElement(By.name("commit")).click();
	}	

}
