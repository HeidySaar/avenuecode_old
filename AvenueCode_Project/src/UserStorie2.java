import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;

public class UserStorie2 {
  
  private PageObject LoginSucess;
  private WebDriver driver;
  private String baseUrl;
  
  @Before
  public void setUp() throws Exception {
    driver = new FirefoxDriver();
    baseUrl = "http://qa-test.avenuecode.com/users/sign_in";
    this.LoginSucess = new PageObject(driver);
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void testAvenue() throws Exception {
	LoginSucess.Open();
	//Logger
	LoginSucess.Login("heidy_saar@hotmail.com", "avenue123");    
	
    assertEquals("(0) Manage Subtasks", driver.findElement(By.xpath("//td[4]/button")).getText());
    driver.findElement(By.id("//td[4]/button")).click();
    driver.findElement(By.id("new_sub_task")).sendKeys("Subtask 1");
    driver.findElement(By.id("add-subtask")).click();
    driver.findElement(By.cssSelector("div.modal-footer.ng-scope > button.btn.btn-primary")).click();
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
  }
}
